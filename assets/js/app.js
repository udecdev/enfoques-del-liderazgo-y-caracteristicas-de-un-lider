window.onload = function() {
    // Código a ejecutar cuando se ha cargado toda la página
 
   // stage2.stop();
    main();
};
var audios = [{
    url: "assets/sounds/click.mp3",
    name: "clic"
}];
ivo.info({
    title: "Universidad de Cindinamarca",
    autor: "Edilson Laverde Molina",
    date: "",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
function main() {
    var t = null;
    udec = ivo.structure({
        created: function () {
            t = this;
            t.events();
            t.animations();
            //precarga audios//
            ivo.load_audio(audios, onComplete = function () {
                ivo("#preload").hide();
                stage1.play();
            });
        },
        methods: {
            events: function () {
                ivo("#btn-start").on("click", function () {
                    stage1.timeScale(3).reverse();
                    stage2.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo("#btn_start").on("click", function () {
                    stage2.timeScale(3).reverse();
                    stage1.timeScale(1).play();
                    ivo.play("clic");
                });
                var slider=ivo("#slider").slider({
                    slides:'.slides',
                    btn_next:"#btn_next",
                    btn_back:"#btn_prev",
                    rules:function(rule){
                        console.log("rules"+rule);
                        
                    },
                    onMove:function(page){
                        console.log("onMove"+page);
                        if(page==1){
                            ivo(".controls").css("left","370px");
                        }
                        if(page==2 || page==3 || page==4 || page==5 || page==6  ){
                            ivo(".controls").css("left","70px");
                        }
                        if(page==7){
                            ivo(".controls").css("left","370px");
                        }
                        ivo.play("clic");
                    },
                    onFinish:function(){
                    }
                });
            },
            animations: function () {
                stage1 = new TimelineMax();
                
                stage1.append(TweenMax.from("#stage1", .8, {y: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.staggerFrom(".triangulos", .8, {y: 1300,x:400, opacity: 0}, .2), 0);
                stage1.append(TweenMax.from("#stage1_mensage", .8, {x: 1300, opacity: 0}), 0);

                stage2 = new TimelineMax();
                stage2.append(TweenMax.from("#stage2", .8, {y: 1300, opacity: 0}), 0);
                stage2.stop();
                let movil = false;
                let windowWidth = window.innerWidth;
                if (windowWidth < 1024) {
                    movil = true;
                    
                }
                if('ontouchstart' in window || navigator.maxTouchPoints) {
                    movil = true;
                }
                if (movil) {
                    stage2.play();
                }

            }
        }
    });
}